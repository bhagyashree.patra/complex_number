#!/usr/bin/env python3

class Complex:
    def __init__(self, real, imag):
        self.r = real
        self.i = imag

    def add(self, number): # number is another Complex object having r and i attributes
        ''' real = self.r + number.r
            imag = self.i + number.i 
            return Complex(real, imag)''' # this is the same as the line below
        self.r += number.r
        self.i += number.i
        return Complex(self.r, self.i)

num1 = Complex(2, 3)    # (2+3i)
num2 = Complex(-3, 4)    # (-3+4i)

result = num1.add(num2)
print(result.r,"+",str(result.i) + "i")




#     def __str__(self):
#         return '{0.real:.2f}{0.imag:+.2f}i'.format(self)
#     def __add__(self, other):
#         return Complex(self.real + other.real, self.imag + other.imag)