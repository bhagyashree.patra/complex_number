# Complex Addition

In this project, I am adding 2 complex numbers using OOP in Python.

### Steps:
- A complex number contains 2 parts: real and imaginary.
  eg: 2 + 3i

- In order to add 2 complex numbers, we need to add the real and imaginary parts separately.
  eg: a = 2 + 3i, b = -2 + 5i
    a + b = a.real + b.real + a.imag + b.imag
          = 2 + (-2) + (3 + 5) i
          = 0 + 8i
          = 8i